# Eltex Summer School 2k17

## Student:
+ Group: 1
+ Name: Marinkevich Sergey Sergeevich
+ VK: [grayhook](https://vk.com/grayhook)
+ Twitch(vods): [GRay_Hook](https://twitch.tv/gray_hook)

## Works:
1. Makefile
2. Pointers & etc
3. Sort structs
4. Processing files
5. Libs (tag "lab5")
6. Forks. Var #10 (Map and scouts that seeking enemies) (tag "lab6")
7. Pipes. Var #10 (Map and scouts that seeking enemies) (tag "lab7")
    + Special: example of coop using of pipes (using signals and lockf)
    + Another variant #11 (Step-by-step game) (tag "lab7.var")
8. Queues. var #11. (tag "lab8")
9. Semaphores & shared memory. var #13

## Tags (attention):
Started with work #5 i using tags:

+ 'lab5' || 'l5' -- Done work #5
+ 'lab6_1' -- Done childs's funs (work #6)
+ 'lab6' -- Done work #6
+ 'lab7' -- Done work #7
+ 'lab7.var' -- Done work #7 with another variant
+ 'lab8' -- Done work #8
+ 'lab9.ch_v' -- Done work #9

## Compile&deploy:
Just type

    make

It will execute programm after compiling.

Execute:

    make run

You may clean folder (remove exe and *.o) with

    make clean
