#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define MAX_LEN 1024 /* максимальная длина строки */
char** str4ki_read(int count);
void str4ki_write(char **str4ki, int count);
int funccmp(const void *val1, const void *val2);
int len_f_word(char *str4ka);
int min_len_word(char *str4ka);
int q_words(char *str4ka);
int inp_str(char *str4ka, int maxlen);
void out_str(char* string, int length, int number);

int main(int argc, char const *argv[]) {
  int str4ki_c = 3;
  printf("%s", "Plz, type quantity of strings:");
  scanf("%d", &str4ki_c);
  getchar();
  printf("Plz, %d type strings: \n", str4ki_c);
  char** str4ki = str4ki_read(str4ki_c);
  // Quantity of words in first line:
  int q_words_first = q_words(str4ki[0]);
  printf("String %s has %d words\n", str4ki[0], q_words_first);
  // Min len of words
  int min_len = min_len_word(str4ki[0]);
  for (int i = 1; i < str4ki_c; i++) {
    if (min_len_word(str4ki[i]) < min_len) {
      min_len = min_len_word(str4ki[i]);
    }
  }
  printf("%s %d\n", "Min length of word = ", min_len);
  printf("%s\n", "Sorted array:");
  qsort(str4ki, str4ki_c, sizeof(char *), funccmp);
  str4ki_write(str4ki, str4ki_c);
  return 0;
}
int q_words(char *str4ka){
  char *p_first_space = str4ka;
  int k = 1;
  while (strchr(p_first_space, ' ') != 0) {
    p_first_space = strchr(p_first_space, ' ')+1;
    k++;
  }
  return k;
}
int min_len_word(char *str4ka){
  char *p_first_space = str4ka;
  int min = strlen(p_first_space);
  while (strchr(p_first_space, ' ') != 0) {
    if (len_f_word(p_first_space) < min) {
      min = len_f_word(p_first_space);
    }
    p_first_space = strchr(p_first_space, ' ')+1;
  }
  return min;
}
int funccmp(const void *val1, const void *val2){
	char *str4ka1 = (char *)val1;
	char *str4ka2 = (char *)val2;
  int merge = len_f_word(str4ka2) - len_f_word(str4ka1);
	return merge;
}
int len_f_word(char *str4ka){
  int result = 0;
  if (strchr(str4ka, ' ') != 0) {
    result = strlen(str4ka) - strlen(strchr(str4ka, ' '));
  } else {
    result = strlen(str4ka);
  }
  return result;
}
char** str4ki_read(int count){
  char **str4ki;
  str4ki = (char **)malloc(sizeof(char *)*count);
  for (int i = 0; i < count ; i++){
    char buffer[MAX_LEN];
    inp_str(buffer, MAX_LEN);
    buffer[strlen(buffer) - 1] = '\0';
    str4ki[i] = (char *)malloc(sizeof(char)*strlen(buffer));
    strcpy(str4ki[i], buffer);
  }
  return str4ki;
}
int inp_str(char *str4ka, int maxlen){
  fgets(str4ka, MAX_LEN, stdin);
  int len = strlen(str4ka);
  return len;
}
void str4ki_write(char **str4ki, int count){
  for (int i = 0; i < count ; i++){
    out_str(str4ki[i], strlen(str4ki[i]), i);
  }
  printf("\n");
}
void out_str(char* string, int length, int number){
  printf("%d) %s; length: %d;\n", number, string, length);
}
