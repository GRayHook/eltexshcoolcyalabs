#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <time.h>
#include <sys/time.h>

#define MAX_SEQ 10
#define IP_HEADER_SIZE 20

# define ODDBYTE(v)	htons((unsigned short)(v) << 8) // FROM SOURCE https://github.com/iputils/iputils/blob/master/ping.c

unsigned short in_cksum(const unsigned short *addr, register int len, unsigned short csum);
