#include "main.h"

int main(int argc, char const *argv[]) {
  int seq = 0;
  struct timeval tv, tv2;
  char *d_addr = (char *)argv[1];

  int sock = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);

  struct sockaddr_in addr;
  addr.sin_family = AF_INET;
  addr.sin_addr.s_addr = inet_addr(d_addr);

  connect(sock, (struct sockaddr *)&addr, sizeof(addr));

  while (seq < MAX_SEQ) {
    struct icmphdr icmp;
    icmp.type = (u_int8_t)8;
    icmp.code = (u_int8_t)0;
    icmp.checksum = (u_int16_t)0;
    icmp.un.echo.id = (u_int16_t)getpid();
    icmp.un.echo.sequence = htons(seq++);
    long data[5] = { 5411542, 5411542, 5411542, 5411542, 5411542 };

    memcpy(&icmp+1, &data, sizeof(data));
    int icmp_size = (sizeof(icmp) + sizeof(data));
    icmp.checksum = in_cksum((unsigned short *)&icmp, icmp_size, 0);
    int s = send(sock, &icmp, icmp_size, 0);

    gettimeofday(&tv, NULL);
    double trip_start = tv.tv_sec * 1000000 + tv.tv_usec;

    if (seq == 1) {
      printf("PING: size=%d addr=%s\n", s + IP_HEADER_SIZE, d_addr);
    }
    
    int boolka = 1;
    while(boolka){
      char buf[s + IP_HEADER_SIZE];
      int n = recv(sock,buf,s + IP_HEADER_SIZE,0);
      struct iphdr *ip_hdr = (struct iphdr *)buf;
      struct icmphdr *icmp_hdr = (struct icmphdr *)((char *)ip_hdr + (4 * ip_hdr->ihl));

      int boolo4ka = icmp_hdr->type == 0 &&
                     icmp_hdr->code == 0 &&
                     icmp_hdr->un.echo.id == getpid();
      if (boolo4ka) {
        printf("PONG: size=%d addr=%s ttl=%d seq=%d ",
                            n, d_addr,
                            ip_hdr->ttl,
                            ntohs(icmp_hdr->un.echo.sequence));

        gettimeofday(&tv2, NULL);
        double trip_stop = tv2.tv_sec * 1000000 + tv2.tv_usec;
        double trip_time = trip_stop - trip_start;
        printf("in %f ms\n", trip_time / 1000);

        boolka = 0;
      }
    }
    sleep(1);
  }
  close(sock);
  return 0;
}

// FROM SOURCE https://github.com/iputils/iputils/blob/master/ping.c :
unsigned short
in_cksum(const unsigned short *addr, register int len, unsigned short csum)
{
	register int nleft = len;
	const unsigned short *w = addr;
	register unsigned short answer;
	register int sum = csum;

	/*
	 *  Our algorithm is simple, using a 32 bit accumulator (sum),
	 *  we add sequential 16 bit words to it, and at the end, fold
	 *  back all the carry bits from the top 16 bits into the lower
	 *  16 bits.
	 */
	while (nleft > 1)  {
		sum += *w++;
		nleft -= 2;
	}

	/* mop up an odd byte, if necessary */
	if (nleft == 1)
		sum += ODDBYTE(*(unsigned char *)w); /* le16toh() may be unavailable on old systems */

	/*
	 * add back carry outs from top 16 bits to low 16 bits
	 */
	sum = (sum >> 16) + (sum & 0xffff);	/* add hi 16 to low 16 */
	sum += (sum >> 16);			/* add carry */
	answer = ~sum;				/* truncate to 16 bits */
	return (answer);
}
