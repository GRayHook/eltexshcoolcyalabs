#include "client2.h"

int main(int argc, char const *argv[]) {
  srand(getpid());
  int sock = wait_for_serv();
  netw_brdcst_msg msg;

  pthread_t worker;
  pthread_create(&worker, NULL, handler_prompt, &sock);
  pthread_detach(worker);

  while(1){
    // Recieve control message from server:
    if(pthread_mutex_lock(&mutex) == 0){
      recvfrom(sock, &msg, sizeof(netw_brdcst_msg), 0, NULL, NULL);
      if (msg.msg == HAVE_MSGS) {
        queue_isnt_empty = 1;
      } else if (msg.msg == MSGS_OVER) {
        queue_isnt_empty = 0;
      }
      pthread_mutex_unlock(&mutex);
      sleep(msg.interval);
    }

    if (msg.msg == DISCONNECT) {
      break;
    }

  }

  return 0;
}

void *handler_prompt(void *args) { // If recv request for messages
  while(1){
    if(pthread_mutex_lock(&mutex) == 0){
      if (queue_isnt_empty) {
        work_prompt();
        pthread_mutex_unlock(&mutex);
      } else {
        pthread_mutex_unlock(&mutex);
      }
    }
    nanosleep (&tw, &tr);
  }

  pthread_exit(0);
}
void work_prompt() { // If recv request for messages
  uint8_t *buf;
  NetwMsg *msg;
  int len;
  int n;
  int sock = socket(AF_INET, SOCK_STREAM, 0);
  if(sock < 0) {
      perror("socket");
      exit(ERR_SOCKET);
  }
  if(connect(sock, (struct sockaddr *)&addr_serv, sizeof(addr_serv)) < 0) {
    perror("connect");
    exit(ERR_CONNECT);
  }

  char qq = RECV; // Negotiate with server, that we want to receive NetwMsg
  send(sock, &qq, sizeof(char), 0);

  n = recv(sock, &len, sizeof(int), 0);
  if (n < 0) {
    perror("recv_msg");
    exit(ERR_RECV_MSG);
  }

  buf = (uint8_t *)malloc(len);

  n = recv(sock, buf, len, 0);
  if (n < 0) {
    perror("recv");
    exit(ERR_RECV_MSG);
  }

  msg = netw_msg__unpack(NULL, len, buf);
  if (msg == NULL) {
    perror("netw_msg__unpack");
    exit(ERR_UNPACK);
  }

  print_NetwMsg(*msg);
  printf("\n");

  netw_msg__free_unpacked(msg, NULL);
  free(buf);
  sleep(msg->sleep);
  close(sock);
}
