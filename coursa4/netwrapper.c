#include "netwrapper.h"
#include <unistd.h>

int wait_for_serv(/* arguments */) {
  int sock = socket(AF_INET, SOCK_DGRAM, 0);
  if(sock < 0) {
    perror("socket");
    exit(1);
  }
  int portnum = SOCKET_PORT;
  addr.sin_family = AF_INET;
  addr.sin_port = htons(portnum);
  addr.sin_addr.s_addr = htonl(INADDR_ANY);
  while (bind(sock, (struct sockaddr *)&addr, sizeof(struct sockaddr_in)) < 0) {
    if (portnum < SOCKET_UPTO_PORT) {
      addr.sin_port = htons(++portnum); // get random port
    } else {
      perror("bind");
      exit(3);
    }
  }
  printf("bind\n");
  netw_brdcst_msg msg;
  socklen_t addr_serv_len = sizeof(addr_serv);
  recvfrom(sock, (void *)&msg, sizeof(netw_brdcst_msg), 0, (struct sockaddr *)&addr_serv, &addr_serv_len);
  addr_serv.sin_port = htons(msg.port);
  printf("Got hello from %d\n", msg.port);
  return sock;
}

void print_NetwMsg(NetwMsg msg) {
  printf("[%d; %d; \"%s\"]", msg.sleep, msg.length, msg.txt);
}
