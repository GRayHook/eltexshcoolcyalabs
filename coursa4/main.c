#include "main.h"

int main(int argc, char const *argv[]) {
  initscr(); // Init curse-mode
  refresh(); // Refresh screen
  pthread_t handle_conns;
  pthread_t msg_distrib1;
  pthread_t msg_distrib2;
  addrs_clnts = (struct sockaddr_in *)malloc(sizeof(struct sockaddr_in));
  queue_i = -1;
  rows = getmaxy(stdscr); // Getting size of terminal
  cols = getmaxx(stdscr); // Getting size of terminal
  addr.sin_family = AF_INET;
  addr.sin_port = htons(0);
  addr.sin_addr.s_addr = htonl(INADDR_ANY);
  sock_ctlsndr = socket(AF_INET, SOCK_DGRAM, 0);
  int enable = 1;
  setsockopt(sock_ctlsndr, SOL_SOCKET, SO_BROADCAST, &enable, sizeof(enable));
  if(sock_ctlsndr < 0) {
    endwin();
    perror("socket");
    exit(ERR_SOCKET);
  }
  if (argc > 1) {
    clients_broadcast_address = inet_addr(argv[1]);
  } else {
    clients_broadcast_address = htonl(INADDR_BROADCAST);
  }

  print_ncur((char*)"QUEUE:", 0, 0);
  print_ncur((char*)"HOSTS:", 0, cols - 11);

  pthread_create(&handle_conns, NULL, handle_connections, NULL);
  pthread_detach(handle_conns);

  msg_for_distr[0].msg = RQ_MSGS;
  msg_for_distr[1].msg = NONEMSG;
  msg_for_distr[0].interval = INTERVAL_K;
  msg_for_distr[1].interval = INTERVAL_L;
  int msg1 = 0, msg2 = 1;

  pthread_create(&msg_distrib1, NULL, ctlmsg_sender, &msg1);
  pthread_detach(msg_distrib1);
  pthread_create(&msg_distrib2, NULL, ctlmsg_sender, &msg2);
  pthread_detach(msg_distrib2);

  mvwprintw(stdscr, rows - 1, 5, "Press any key to exit...");
  getch(); // Wait for any key
  close(sock_ctlsndr);
  endwin();
  return 0;
}

void *ctlmsg_sender(void *argv) {
  int msg_i = *(int *)argv;
  struct sockaddr_in addr_clnt;
  addr_clnt.sin_family = AF_INET;
  addr_clnt.sin_addr.s_addr = clients_broadcast_address;

  sleep(1);
  while(1) {
    if (msg_for_distr[msg_i].msg != NONEMSG) {
      for (int i = SOCKET_PORT; i < SOCKET_UPTO_PORT; i++) {
        addr_clnt.sin_port = htons(i);
        int n = sendto(sock_ctlsndr, &msg_for_distr[msg_i], sizeof(netw_brdcst_msg), 0,
        (struct sockaddr *)&addr_clnt, sizeof(addr_clnt));
        if (n < 0) {
          endwin();
          perror("send");
          exit(ERR_SENDTO);
        }
      }
    }
    sleep(msg_for_distr[msg_i].interval);
  }
}

void *handle_connections(void *args) { // For incoming connections
  int sock;
  sock = socket(AF_INET, SOCK_STREAM, 0);
  if(sock < 0) {
      endwin();
      perror("socket");
      exit(ERR_SOCKET);
  }
  if(bind(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
    endwin();
    perror("bind");
    exit(ERR_BIND);
  }
  listen(sock, 1);
  socklen_t socklen = sizeof(addr_serv);
  if (getsockname(sock, (struct sockaddr *)&addr_serv, &socklen) == -1) {
    endwin();
    perror("getsockname");
    exit(ERR_GETSOCKNAME);
  }

  msg_for_distr[0].port = ntohs(addr_serv.sin_port);
  msg_for_distr[1].port = ntohs(addr_serv.sin_port);

  while (1) {
    pthread_t handler;
    struct sockaddr_in addr_clnt;
    socklen_t addr_clnt_len = sizeof(addr_clnt);
    int conn = accept(sock, (struct sockaddr *)&addr_clnt, &addr_clnt_len);
    if(conn < 0) {
      endwin();
      perror("accept");
      exit(ERR_ACCEPT);
    }

    if (get_host_id(addr_clnt) == -1) {
      clients++;
      addrs_clnts = (struct sockaddr_in *)realloc(addrs_clnts, clients * sizeof(struct sockaddr_in));
      addrs_clnts[clients - 1] = addr_clnt;
      char txt[2];
      sprintf(txt, "%d", clients);
      print_ncur(txt, 0, cols - 3);
    }

    pthread_create(&handler, NULL, client_handler, (void *)&conn);
    nanosleep (&tw, &tr); // wait while thread getting conn
    pthread_detach(handler);
  }

  close(sock);
  pthread_exit(0);
}
void *client_handler(void *args) { // For each client
  int conn = *(int *)args;
  char qq;

  int n = recv(conn, &qq, sizeof(char), 0); // What client want to do
  if (n < 0) {
    endwin();
    perror("recv_qq");
    exit(ERR_RECV_QQ);
  }

  switch (qq) {
    case SEND:
      if (queue_i < QUEUE_SIZE - 1) {
        client_send(conn);
      }
      break;
    case RECV:
      if (queue_i >= 0) {
        client_recv(conn);
      }
      break;
  }
  print_queue();

  close(conn);
  pthread_exit(0);
}
int client_send(int conn) {
  uint8_t *buf;
  NetwMsg *msg;
  int len;

  int n = recv(conn, &len, sizeof(int), 0);
  if (n < 0) {
    endwin();
    perror("recv_msg");
    exit(ERR_RECV_MSG);
  }
  buf = (uint8_t *)malloc(len);

  n = recv(conn, buf, len, 0);
  if (n < 0) {
    endwin();
    perror("recv_msg");
    exit(ERR_RECV_MSG);
  }
  msg = netw_msg__unpack(NULL, len, buf);
  if (msg == NULL) {
    perror("netw_msg__unpack");
    exit(ERR_UNPACK);
  }


  if (pthread_mutex_lock(&queue_mutext) == 0) {
    int sa = queue_push(msg);
    if (sa == 0) {
      print_ncur((char *)"enough", rows - 3, 2);
      msg_for_distr[0].msg = ENOUGH_MSGS;
      msg_for_distr[1].msg = HAVE_MSGS;
    } else {
      print_ncur((char *)"      ", rows - 3, 2);
      msg_for_distr[0].msg = RQ_MSGS;
      msg_for_distr[1].msg = HAVE_MSGS;
    }
    pthread_mutex_unlock(&queue_mutext);
  }

  netw_msg__free_unpacked(msg, NULL);
  free(buf);
  return 0;
}
int client_recv(int conn) {
  int n;
  NetwMsg *buf = (NetwMsg *)malloc(sizeof(NetwMsg));
  if (pthread_mutex_lock(&queue_mutext) == 0) {
    if (!queue_pop(buf)) {
      print_ncur((char *)"empty  ", rows - 3, 2);
      msg_for_distr[0].msg = RQ_MSGS;
      msg_for_distr[1].msg = MSGS_OVER;
    } else {
      print_ncur((char *)"       ", rows - 3, 2);
      msg_for_distr[0].msg = RQ_MSGS;
      msg_for_distr[1].msg = HAVE_MSGS;
    }
    pthread_mutex_unlock(&queue_mutext);
  }

  int len = netw_msg__get_packed_size(buf);
  uint8_t *packed = (uint8_t *)malloc(len);
  netw_msg__pack(buf, packed);

  n = send(conn, &len, sizeof(int), 0);
  if (n < 0) {
    endwin();
    perror("send_len");
    exit(ERR_SEND);
  }
  n = send(conn, packed, sizeof(NetwMsg), 0);
  if (n < 0) {
    endwin();
    perror("send_packed");
    exit(ERR_SEND);
  }

  free(buf->txt);
  free(buf);
  free(packed);
  return 0;
}

void print_queue() {
  for (int i = 0; i <= queue_i; i++) {
    char txt[MAX_LENGTH + 10];
    sprintf(txt, "[%d; %d; \"%s\"]", queue[i].sleep, queue[i].length, queue[i].txt);
    print_fillspaces_ncur(2 + i);
    print_ncur(txt, 2 + i, 0);
  }
  for (int i = queue_i + 1; i < QUEUE_SIZE; i++) {
    print_fillspaces_ncur(2 + i);
  }
}
int queue_push(NetwMsg *msg) {
  if (queue_i < QUEUE_SIZE - 1) {
    memcpy(&queue[++queue_i], msg, sizeof(NetwMsg));
    queue[queue_i].txt = (char *)malloc(sizeof(char) * strlen(msg->txt) + 1);
    strcpy(queue[queue_i].txt, msg->txt);
    if (queue_i == QUEUE_SIZE - 1) {
      return 0;
    }
    return 1;
  }
  return -1;
}
int queue_pop(NetwMsg *msg) {
  memcpy(msg, &queue[0], sizeof(NetwMsg));
  msg->txt = (char *)malloc(sizeof(char) * strlen(queue[0].txt) + 1);
  strcpy(msg->txt, queue[0].txt);
  free(queue[0].txt);

  for (int i = 1; i <= queue_i; i++) {
    queue[i - 1] = queue[i];
  }
  memset(&queue[queue_i--], 0, sizeof(NetwMsg));

  if (queue_i < 0) {
    return 0;
  }
  return 1;
}

int get_host_id(struct sockaddr_in addr_clnt) {
  for (int i = 0; i < clients; i++) {
    if (ntohl(addrs_clnts[i].sin_addr.s_addr) == ntohl(addr_clnt.sin_addr.s_addr)) {
      return i;
    }
  }
  return -1;
}

void print_fillspaces_ncur(int x) {
  for (int i = 0; i < MAX_LENGTH + 10; i++) {
    print_ncur((char *)" ", x, i);
  }
}
void print_ncur(char *txt, int x, int y){ // Print by ncurses
  if (pthread_mutex_lock(&graphen_mutext) == 0) {
    mvwprintw(stdscr, x, y, "%s", txt);
    refresh();
    pthread_mutex_unlock(&graphen_mutext);
  }
}
