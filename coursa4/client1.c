#include "client1.h"

int main(int argc, char const *argv[]) {
  srand(getpid());
  int sock = wait_for_serv();
  netw_brdcst_msg msg;

  pthread_t worker;
  pthread_create(&worker, NULL, handler_prompt, &sock);
  pthread_detach(worker);

  while(1){
    // Recieve control message from server:
    if(pthread_mutex_lock(&mutex) == 0){
      recvfrom(sock, &msg, sizeof(netw_brdcst_msg), 0, NULL, NULL);
      if (msg.msg == RQ_MSGS) {
        queue_isnt_full = 1;
      } else if (msg.msg == ENOUGH_MSGS) {
        queue_isnt_full = 0;
      }
      pthread_mutex_unlock(&mutex);
      sleep(msg.interval);
    }

    if (msg.msg == DISCONNECT) {
      break;
    }
  }

  return 0;
}

void *handler_prompt(void *args) { // If recv request for messages
  while(1){
    if(pthread_mutex_lock(&mutex) == 0){
      if (queue_isnt_full) {
        work_prompt();
        pthread_mutex_unlock(&mutex);
      } else {
        pthread_mutex_unlock(&mutex);
      }
    }
    nanosleep(&tw, &tr);
  }

  pthread_exit(0);
}
void work_prompt() { // If recv request for messages
  int sock = socket(AF_INET, SOCK_STREAM, 0);
  if(sock < 0) {
      perror("socket");
      exit(1);
  }
  if(connect(sock, (struct sockaddr *)&addr_serv, sizeof(addr_serv)) < 0) {
    perror("connect");
    exit(4);
  }

  NetwMsg buf = NETW_MSG__INIT;
  buf.length = rand() % (MAX_LENGTH - 1) + 1;
  buf.sleep = rand() % (MAX_TIME - 1) + 1;
  char str4ka[MAX_LENGTH] = "";
  for(int i = 0; i < buf.length; i++){
    str4ka[i] = (rand() % 23) + 64 + (rand() % 2)*33;
  }
  buf.txt = (char *)malloc(sizeof(char) * buf.length + 1);
  strcpy(buf.txt, str4ka);

  int len = netw_msg__get_packed_size(&buf);
  uint8_t *packed = (uint8_t *)malloc(len);
  netw_msg__pack(&buf, packed);

  print_NetwMsg(buf);
  printf("\n");

  char qq = SEND; // Negotiate with server, that we want to send NetwMsg
  send(sock, &qq, sizeof(char), 0);
  send(sock, &len, sizeof(int), 0);
  send(sock, packed, len, 0);

  free(buf.txt);
  free(packed);
  sleep(buf.sleep);
  close(sock);
}
