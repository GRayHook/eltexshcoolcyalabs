#include <pthread.h>
#include <ncurses.h>
#include <arpa/inet.h>
#include "netwrapper.h"

#define  QUEUE_SIZE 10
#define  INTERVAL_K 1
#define  INTERVAL_L 2

pthread_mutex_t graphen_mutext = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t queue_mutext = PTHREAD_MUTEX_INITIALIZER;
int sock_ctlsndr;
struct sockaddr_in *addrs_clnts;
NetwMsg queue[QUEUE_SIZE];
int queue_i, clients;
netw_brdcst_msg msg_for_distr[2];
int rows, cols;
struct timespec tr;
unsigned long clients_broadcast_address;

struct timespec tw = {0,1000000};

void *ctlmsg_sender(void *argv);

void *handle_connections(void *args);
void *client_handler(void *args);

int client_send(int conn);
int client_recv(int conn);

void print_queue();
int queue_push(NetwMsg *msg);
int queue_pop(NetwMsg *msg);

int get_host_id(struct sockaddr_in addr_clnt);

void print_fillspaces_ncur(int x);
void print_ncur(char *txt, int x, int y);
