run: main
	xterm -geometry 30x15+800+0 -T "Client 1" -fs 14 -fa FreeType -e './coursa4_client1 && echo "Press any key..." && read -n 1' &
	xterm -geometry 30x15+400+0 -T "Client 1" -fs 14 -fa FreeType -e './coursa4_client1 && echo "Press any key..." && read -n 1' &
	xterm -geometry 30x15+200+0 -T "Client 2" -fs 14 -fa FreeType -e './coursa4_client2 && echo "Press any key..." && read -n 1' &
	xterm -geometry 30x15+200+500 -T "Client 1" -fs 14 -fa FreeType -e './coursa4_client1 && echo "Press any key..." && read -n 1' &
	xterm -geometry 30x15+800+0 -T "Client 1" -fs 14 -fa FreeType -e './coursa4_client1 && echo "Press any key..." && read -n 1' &
	./coursa4_server

main: main.c libnetwrapper.a libprotobuf.pb-c.a
	gcc main.c -g -Wall -std=c99 -D_GNU_SOURCE -o coursa4_server -lpthread -lncurses -L. -lnetwrapper -lprotobuf.pb-c -lprotobuf-c
	gcc client1.c -g -Wall -std=c99 -D_GNU_SOURCE -o coursa4_client1 -L. -lnetwrapper -lpthread -lprotobuf.pb-c -lprotobuf-c
	gcc client2.c -g -Wall -std=c99 -D_GNU_SOURCE -o coursa4_client2 -L. -lnetwrapper -lpthread -lprotobuf.pb-c -lprotobuf-c

libnetwrapper.a: netwrapper.o
	ar cr libnetwrapper.a netwrapper.o

libprotobuf.pb-c.a: protobuf.pb-c.o
	ar cr libprotobuf.pb-c.a protobuf.pb-c.o

netwrapper.o: netwrapper.c
	gcc -g -Wall -std=c99 -D_GNU_SOURCE -lpthread -c netwrapper.c -o netwrapper.o

protobuf.pb-c.o: protobuf.pb-c.c
	gcc -g -Wall -std=c99 -D_GNU_SOURCE -c protobuf.pb-c.c -o protobuf.pb-c.o

clean:
	rm -f coursa4* *.a *.o *.gch
