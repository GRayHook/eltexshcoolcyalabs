#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include "protobuf.pb-c.h"

#define  MAX_LENGTH 25
#define  MAX_TIME 5
#define  SOCKET_PORT 51137
#define  SOCKET_UPTO_PORT 51147
#define  DISCONNECT 19
#define  ACCEPT_CONN 7
#define  NONEMSG 1
#define  RQ_MSGS 2
#define  ENOUGH_MSGS 3
#define  HAVE_MSGS 4
#define  MSGS_OVER 5
#define  SEND 21
#define  RECV 22

#define  ERR_SOCKET 1
#define  ERR_BIND 2
#define  ERR_ACCEPT 3
#define  ERR_CONNECT 4
#define  ERR_SENDTO 11
#define  ERR_SEND 12
#define  ERR_GETSOCKNAME 21
#define  ERR_RECV_QQ 31
#define  ERR_RECV_MSG 32
#define  ERR_UNPACK 41

typedef struct struct_sock_addr {
  int sock;
  struct sockaddr_in addr;
} sock_addr;
// typedef struct struct_NetwMsg {
//   int sleep;
//   int length;
//   char txt[MAX_LENGTH];
// } NetwMsg;
typedef struct struct_netw_brdcst_msg {
  int port;
  char msg;
  int interval;
} netw_brdcst_msg;

struct sockaddr_in addr;
struct sockaddr_in addr2;
struct sockaddr_in addr_serv;
pthread_mutex_t mutex;

int wait_for_serv(/* arguments */);
void print_NetwMsg(NetwMsg msg);
