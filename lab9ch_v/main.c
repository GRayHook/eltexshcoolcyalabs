#include "main.h"

int main(int argc, char const *argv[]) {
  int semid = sem_init();
  int shmid = shm_init();
  int *chpids = shm_at(shmid);

  for (int i = 0; i < K_PLAYERS; i++) {
    pid_t chpid = fork();
    if (chpid == 0) {
      return player_handler(semid, shmid);
    } else {
      chpids[i] = chpid;
    }
  }
  return parent_handler(semid, shmid);
}

int player_handler(int semid, int shmid){
  int *chpids = shm_at(shmid);
  struct sembuf lock_res = {0, -1, 0};
  struct sembuf rel_res = {0, 1, 0};
  int bro;
  long chpids_sum = 0;
  srand(getpid());
  sleep(1);
  while (chpids_sum != (long)getpid()) { // Until every body die
    chpids_sum = 0;
    semop(semid, &lock_res, 1);
    do {
      bro = rand() % K_PLAYERS;
    } while(chpids[bro] == getpid() || chpids[bro] == 0);
    kill(chpids[bro], SIGKILL);
    printf("CHILD(%ld): kill %ld\n", (long)getpid(), (long)chpids[bro]);
    fflush(stdout);
    chpids[bro] = 0;
    semop(semid, &rel_res, 1);
    sleep(1);
    for (int i = 0; i < K_PLAYERS; i++) {
      chpids_sum += (long)chpids[i];
    }
  }

  shmdt(chpids);
  return 0;
}
int parent_handler(int semid, int shmid){
  int *chpids = shm_at(shmid);
  check_childs(chpids);
  for (int i = 0; i < K_PLAYERS; i++) {
    if (chpids[i]) {
      printf("PARENT: WINNER IS %d!!!\n", chpids[i]);
    }
  }

  shmdt(chpids);
  return 0;
}

pid_t *shm_at(int shmid){
  pid_t *chpids;
  if ((chpids = (pid_t *)shmat(shmid, NULL, 0)) == (pid_t *) -1) {
          perror("shmat");
          exit(1);
  }
  return chpids;
}
int sem_init(){
  int semid = semget(IPC_PRIVATE, 1, 0666 | IPC_CREAT);
  union semun arg;
  arg.val = 1;
  semctl(semid, 0, SETVAL, arg);
  return semid;
}
int shm_init() {
  int shmid;
  if ((shmid = shmget(IPC_PRIVATE, sizeof(pid_t) * K_PLAYERS, 0666 | IPC_CREAT)) < 0) {
          perror("shmget");
          exit(1);
  }
  return shmid;
}

void check_childs(pid_t *chpids){
  int status;
  for (int i = 0; i < K_PLAYERS; i++) {
      waitpid(chpids[i], &status, WUNTRACED);
  }
}
