#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/wait.h>
#define  K_PLAYERS 5

union semun {
 int val;
 struct semid_ds *buf;
 unsigned short *array;
 struct seminfo *__buf;
};

int player_handler(int semid, int shmid);
int parent_handler(int semid, int shmid);

pid_t *shm_at(int shmid);
int sem_init();
int shm_init();

void check_childs(pid_t *chpids);
