#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/wait.h>
#include <ncurses.h>

#include "player.h"
#include "map.h"
#define  HEIGHT 8
#define  WIDTH 10
#define  K_PLAYERS 5
#define  K_DIMENTIONS 2
#define  T_DELAY 200000000
#define  T_DELAY2 100000000
#define  MAX_SEND_SIZE 1000
struct plrmsgbuf {
        long mtype;
        player plr;
};
struct notsigmsgbuf {
        long mtype;
        int notsig;
};

int map_handler(pid_t *chpids, maps *map, int msqid);
int player_handler(maps *map, int msqid);
void duel_kill(player **players, int i, int j, int msqid);
void send_msg(int msqid, player *player);
player *recieve_msg(int msqid);
void not_signal(int msqid, pid_t pid, int notsig);
int check_childs(pid_t *chpids);
