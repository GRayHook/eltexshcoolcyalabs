#include "player.h"
#include "map.h"

void player_toMove(player *plr, int *xy){
  plr->coord[0] = plr->coord[0] + xy[0];
  plr->coord[1] = plr->coord[1] + xy[1];
}
int player_toLookForEnemies(maps *map, player *plr){
  if (map->map[plr->coord[0]][plr->coord[1]] == 1){
    plr->HP++;
    return 1;
  }
  return 0;
}
player *player_toCreateNInit(maps *map) {
  srand(getpid());
  player *plr = (player *)malloc(sizeof(player));
  plr->coord[0] = rand() % map->mn[0];
  plr->coord[1] = rand() % map->mn[1];
  int *drisnya = chooseDirection(map, plr);
  plr->direction[0] = (int)drisnya[0];
  plr->direction[1] = (int)drisnya[1];
  plr->HP = 1;
  plr->pid = getpid();
  plr->gamover = 0;
  return plr;
}
int *chooseDirection(maps *map, player *plr){
  // Look where we are and return direction
  int paths[K_DIRECTIONS];
  paths[0] = plr->coord[0]; // up
  paths[1] = map->mn[1] - plr->coord[1];
  paths[2] = map->mn[0] - plr->coord[0];
  paths[3] = plr->coord[1];
  int max = 0;
  for (int i = 1; i < K_DIRECTIONS; i++) {
    if (paths[i] > paths[max]) {
      max = i;
    }
  }
  int *direction = (int *)malloc(sizeof(int) * K_DIMENTIONS);
  switch (max) {
    case 0:
      direction[0] = -1;
      break;
    case 1:
      direction[1] = 1;
      break;
    case 2:
      direction[0] = 1;
      break;
    case 3:
      direction[1] = -1;
      break;
  }
  return direction;
}
