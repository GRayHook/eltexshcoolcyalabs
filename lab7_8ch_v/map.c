#include "map.h"

int map_isEdge(maps *map, int *coordsv, int *direction){
  if ((coordsv[0] == (map->mn[0] - 1) || coordsv[0] == 0) && direction[0] != 0) {
    return 1;
  }
  if ((coordsv[1] == (map->mn[1] - 1) || coordsv[1] == 0) && direction[1] != 0) {
    return 1;
  }
  return 0;
}
void map_toPrint(maps *map) {
  for (int i = 0; i < map->mn[0]; i++) {
    for (int j = 0; j < map->mn[1]; j++) {
      switch (map->map[i][j]) {
        case -1:
          mvwprintw(stdscr, i, j * POINT_WIDTH, "%d ", map->map[i][j]);
          break;
        case 1:
          mvwprintw(stdscr, i, j * POINT_WIDTH, "+%d ", map->map[i][j]);
          break;
        case 0:
          mvwprintw(stdscr, i, j * POINT_WIDTH, " %d ", map->map[i][j]);
          break;
      }
    }
  }
  refresh();
}
void map_toPrint_higlight(maps *map, int **coordsv, int *HPs, int coordsc) {
  int xy[K_DIMENTIONS];
  for (int i = 0; i < map->mn[0]; i++) {
    xy[0] = i;
    for (int j = 0; j < map->mn[1]; j++) {
      xy[1] = j;
      int k = inArr(xy, coordsv, coordsc);
      if (-1 < k) {
        if (HPs[k] > 0) {
          mvwprintw(stdscr, i, j * POINT_WIDTH, "[%d]", HPs[k]);
        } else {
          mvwprintw(stdscr, i, j * POINT_WIDTH, "[X]");
        }
      } else {
        switch (map->map[i][j]) {
          case -1:
            mvwprintw(stdscr, i, j * POINT_WIDTH, "%d ", map->map[i][j]);
            break;
          case 1:
            mvwprintw(stdscr, i, j * POINT_WIDTH, "+%d ", map->map[i][j]);
            break;
          case 0:
            mvwprintw(stdscr, i, j * POINT_WIDTH, " %d ", map->map[i][j]);
            break;
        }
      }
    }
    refresh();
  }
}
int inArr(int *xy, int **coordsv, int coordsc){
  for (int i = 0; i < coordsc; i++) {
    if (xy[0] == coordsv[i][0] && xy[1] == coordsv[i][1]) {
      return i;
    }
  }
  return -1;
};
int **map_new(int *mn){
  int **map = (int **)malloc(sizeof(int *) * mn[0]);
  for (int i = 0; i < mn[0]; i++) {
    map[i] = (int *)malloc(sizeof(int) * mn[1]);
  }
  map_toGenerate(map, mn);
  return map;
}
void map_toFree(maps *map) {
  for (int i = 0; i < map->mn[0]; i++) {
    free(map->map[i]);
  }
  free(map->map);
}
void map_toGenerate(int **map, int *mn){
  int random;
  srand(getpid());
  for (int i = 0; i < mn[0]; i++) {
    for (int j = 0; j < mn[1]; j++) {
      random = rand() % 100;
      map[i][j] = map_rand(random);
    }
  }
}
int map_rand(int random) {
  if (random > MAP_CHANCE) {
    return 0;
  } else if (random > (MAP_CHANCE / 2)) {
    return -1;
  } else {
    return 1;
  }
}
