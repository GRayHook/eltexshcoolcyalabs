#ifndef PLAYER_H
#define PLAYER_H

#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include "map.h"
#define  K_DIRECTIONS 4
#define  K_DIMENTIONS 2

struct strct_player{
  pid_t pid;
  int coord[K_DIMENTIONS];
  int direction[K_DIMENTIONS];
  int HP;
  int gamover;
};
typedef struct strct_player player;

int player_toLookForEnemies(maps *map, player *plr);
void player_toMove(player *plr, int *xy);
player *player_toCreateNInit(maps *map);
int *chooseDirection(maps *map, player *plr);

#endif
