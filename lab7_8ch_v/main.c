#include "main.h"

int main(int argc, char const *argv[]) {
  maps *map = (maps *)malloc(sizeof(maps));
  map->mn[0] = HEIGHT;
  map->mn[1] = WIDTH;
  map->map = map_new(map->mn);

  int msqid = msgget(IPC_PRIVATE, 0600|IPC_CREAT);

  pid_t *chpids = (pid_t *)malloc(sizeof(pid_t) * K_PLAYERS);
  pid_t chpid;
  for (int i = 0; i < K_PLAYERS; i++) {
    chpid = fork();
    if (chpid == 0) {
      player_handler(map, msqid);
      return 0;
    } else {
      (i + 3)[chpids - 3] = chpid;
    }
  }
  sleep(1);
  return map_handler(chpids, map, msqid);
}

int map_handler(pid_t *chpids, maps *map, int msqid){
  initscr(); // Init curse-mode
  refresh(); // Refresh screen
  int good_ended; // Counter for ended players
  int rows; // Sizes of terminal
  rows = getmaxy(stdscr); // Getting size of terminal
  map_toPrint(map); // Print empty map
  player **players = (player **)malloc(sizeof(player *) * K_PLAYERS);
  int **coords = (int **)malloc(sizeof(int *) * K_PLAYERS);//Coords for tracking
  int *HPs = (int *)malloc(sizeof(int) * K_PLAYERS); // Array of health points
  // Get places of players:
  for (int i = 0; i < K_PLAYERS; i++) { // For each player:
    players[i] = (player *)malloc(sizeof(player));
    players[i] = recieve_msg(msqid); // Recieve each player from pipe
    coords[i] = players[i]->coord; // Remember coords
    HPs[i] = players[i]->HP; // Remember HP
  }
  map_toPrint_higlight(map, coords, HPs, K_PLAYERS); // Print map with players

  do {
    good_ended = 0;
    for (int i = 0; i < K_PLAYERS; i++) { // For each player:
      if (players[i]->gamover) { // If current player have flag of game over:
        good_ended++; // Increment counter of ended players
      } else { // If current player havent flag of game over:
        not_signal(msqid, players[i]->pid, 1); // Send msg that means plr can
        struct timespec tr;             // make motion
        struct timespec tw = {0,T_DELAY};
        nanosleep (&tw, &tr);
        players[i] = recieve_msg(msqid); // Recieve update for current player
        map->map[players[i]->coord[0]][players[i]->coord[1]] = 0; // Set to zero
        nanosleep (&tw, &tr);                  // value on players coords on map
        for (int k = 0; k < K_PLAYERS; k++) {
          int boolka = players[i]->coord[0] == players[k]->coord[0] &&
                       players[i]->coord[1] == players[k]->coord[1] &&
                       players[k]->gamover == 0;
          if (boolka) { // Duel:
            if (players[i]->HP < players[k]->HP) {
              duel_kill(players, i, k, msqid);
            } else if (players[k]->HP < players[i]->HP) {
              duel_kill(players, k, i, msqid);
            }
          }
        }
        coords[i] = players[i]->coord; // Update coords for map_toPrint_higlight
        HPs[i] = players[i]->HP; // Update HPs for map_toPrint_higlight
        if (players[i]->gamover) { // Inform, if player get game over flag:
          mvwprintw(stdscr, rows - 2 - i, 1, "%ld Gamover.", players[i]->pid);
        }
        map_toPrint_higlight(map, coords, HPs, K_PLAYERS); // Update map
      }
    }
  } while (K_PLAYERS - good_ended); // Until there are players
  // Game over:
  mvwprintw(stdscr, rows - 1, 5, "Press any key...");
  // Free:
  refresh();
  getch(); // Wait for any key
  endwin();
  while (check_childs(chpids)); // Wait childs
  for (size_t i = 0; i < K_PLAYERS; i++) {
    free(players[i]);
  }
  free(players);
  free(coords);
  free(chpids);
  map_toFree(map);
  return 0;
}
int player_handler(maps *map, int msqid){
  player *plr = player_toCreateNInit(map); // Init player
  send_msg(msqid, plr); // Send player's struct for init map
  struct notsigmsgbuf buf;

  do {
    msgrcv(msqid, &buf, sizeof(int), getpid(), 0);
    if (buf.notsig == 1) { // If we get a permission:
      player_toMove(plr, plr->direction); // Move on map
      plr->HP += map->map[plr->coord[0]][plr->coord[1]]; // Apply value from map
      if (map_isEdge(map, plr->coord, plr->direction) || plr->HP < 1) {
        // If player on edge of map or his HP is over:
        plr->gamover = 1;
      } else {
        plr->gamover = 0;
      }
      send_msg(msqid, plr); // Send player's struct to pipe
    } else {
      plr->gamover = 1;
    }

  } while(!plr->gamover); // Until there isn't game over flag
  // Free:
  free(plr);
  return 0;
}

void duel_kill(player **players, int i, int j, int msqid) {
  int rows; // Sizes of terminal
  rows = getmaxy(stdscr); // Getting size of terminal
  players[i]->HP = 0;
  players[i]->gamover = 1;
  not_signal(msqid, players[i]->pid, 2);
  mvwprintw(stdscr, rows - 2 - i, 1,
            "%ld Gamover. Was killed by %ld",
            players[i]->pid, players[j]->pid);
}
void send_msg(int msqid, player *plr) {
  struct plrmsgbuf buf;

  buf.mtype = (long)getppid();
  buf.plr = *plr;
  msgsnd(msqid, &buf, sizeof(player), 0);
}
player *recieve_msg(int msqid) {
  struct plrmsgbuf buf;
  struct timespec tr;
  struct timespec tw = {0,T_DELAY2};
  player *plr = (player *)malloc(sizeof(player));
  msgrcv(msqid, &buf, sizeof(player), (long)getpid(), 0);
  *plr = buf.plr;
  nanosleep (&tw, &tr);

  return plr;
}
void not_signal(int msqid, pid_t pid, int notsig) {
  struct notsigmsgbuf buf;
  buf.mtype = (long)pid;
  buf.notsig = notsig;
  msgsnd(msqid, &buf, sizeof(int), 0);
}
int check_childs(pid_t *chpids){
  int status;
  int statuses = K_PLAYERS;
  for (int i = 0; i < K_PLAYERS; i++) {
      waitpid(chpids[i], &status, WNOHANG | WUNTRACED);
      if (kill(chpids[i], 0)){
        statuses--;
      }
  }
  return statuses;
}
