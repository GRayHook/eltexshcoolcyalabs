#ifndef MAP_H
#define MAP_H

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <ncurses.h>
#include "map.h"
#define  K_DIMENTIONS 2
#define  MAP_CHANCE 40
#define  POINT_WIDTH 3

struct strct_map {
  int **map;
  int mn[2];
  void *plrs;
};
typedef struct strct_map maps;

int map_isEdge(maps *map, int *coordsv, int *direction);
void map_toPrint_higlight(maps *map, int **coordsv, int *HPs, int coordsc);
void map_toPrint(maps *map);
int inArr(int *xy, int **coordsv, int coordsc);
int **map_new(int *mn);
void map_toFree(maps *map);
void map_toGenerate(int **map, int *mn);
int map_rand(int random);

#endif
