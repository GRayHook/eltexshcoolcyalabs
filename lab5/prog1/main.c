#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "moddiv.h"

int main(int argc, char const *argv[]) {
  printf("%d - %d\n", dem_mod(25, 7), dem_div(25, 7));
  return 0;
}
