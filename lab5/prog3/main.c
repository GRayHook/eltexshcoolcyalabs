#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dlfcn.h>
// int dem_div(int divider, int denominator);
// int dem_mod(int divider, int denominator);

int main(int argc, char const *argv[]) {
  int (*dem_div)(int, int);
  int (*dem_mod)(int, int);
  char *filename = (char *)"prog3/libmoddiv.so";
  void *lib = dlopen(filename, RTLD_LAZY);
  dem_div = (int (*)(int, int))dlsym(lib, (char *)"dem_div");
  dem_mod = (int (*)(int, int))dlsym(lib, (char *)"dem_mod");
  printf("%d - %d\n", dem_mod(25, 7), dem_div(25, 7));
  dlclose(lib);
  return 0;
}
