// Copyleft (ↄ) 2017 Marinkevich Sergey (G-Ray_Hook). All lefts reserved.
// Contact: s@marinkevich.ru
// =============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>

int main(int argc, char const *argv[]) {

  int fd[2];
  pipe(fd);

  struct sigaction act;
  sigset_t set; // List of expected signals
  sigemptyset(&set); // Init set
  sigaddset(&set, SIGUSR1); // Add signal USR1 to set
  act.sa_mask = set; // Apply set to mask
  sigaction(SIGUSR1, &act, 0); // Apply empty handler for USR1

  if (fork() == 0) {
    pid_t pid = fork();
    // =========================================================================
    // CHILD{1,2}:
    close(fd[0]);
    int sig; // Buf for gotten signal
    if(lockf(fd[1], F_LOCK, 0) < 0){ // Lock file descriptor
      perror("SETLOCK");
    }
    pid_t mypid = getpid();
    int buffer[3];
    if (pid == 0) {
      buffer[0] = 1;
    } else {
      buffer[0] = 2;
    }
    buffer[1] = (int)((long)mypid % 256); // Put pid in buffer...
    buffer[2] = (int)((long)mypid / 256); // ... like 2 integer
    write(fd[1], buffer, (sizeof(int) * 3));
    sigwait(&set, &sig); // Wait for a signal by parent, recieved our msg
    if(lockf(fd[1], F_ULOCK, 0) < 0){ // Unlock file descriptor...
      perror("F_SETLOCK");           // ... (when we recieved signal)
    }
    printf("drisnya\n");
    return 0;
    // =========================================================================
  } else {
    // =========================================================================
    // PARENT:
    close(fd[1]);
    sleep(1);
    int buffer[3];
    for (size_t i = 0; i < 2; i++) {
      read(fd[0], &buffer, sizeof(int) * 3);
      pid_t anspid = (pid_t)((long)buffer[1] +
                             (long)buffer[2] * 256); // Get pid by 2 int
      printf("%ld-> %d\n", (long)anspid, buffer[0]);
      kill(anspid, SIGUSR1); // Send signal to child, that sent msg, by pid
    }
    return 0;
    // =========================================================================
  }
}
