// Copyleft (ↄ) 2017 Marinkevich Sergey (G-Ray_Hook). All lefts reserved.
// Contact: s@marinkevich.ru
// =============================================================================
#include <stdio.h>

int main(){
char a[] = {'H','e','l','l', // "Hell"
            ' ',' ',' ',' ', // "    "
            'r','l','d','!'};// "rld!"
printf("%s\n", a); // Hell    rld!
int *b = (int *)a;
b[1] = 1870078063; // 1870078063 = 01101111 00100000 01110111 01101111
char *c = (char *)b;
printf("%s\n", c); // Hello world!
return 0;
}
