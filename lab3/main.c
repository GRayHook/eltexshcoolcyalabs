#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define  MAX_LEN 50
#define  MAX_LEN2 70
struct strct_students_dairy;
typedef struct strct_students_dairy students_dairy;
void dairys_print(students_dairy **dairys, int q);
students_dairy **dairys_read(int q);
void input_dairy(students_dairy *dairy);
void dairys_drop(students_dairy **dairys, int q);
int dairys_comparator(const void *val1, const void *val2);

int main(int argc, char const *argv[]) {
  printf("Enter quantity of records: ");
  int q = 0;
  scanf("%d", &q);
  students_dairy **dairys = dairys_read(q);
  qsort(dairys, q, sizeof(students_dairy *), dairys_comparator);
  dairys_print(dairys, q);
  dairys_drop(dairys, q);
  return 0;
}

struct strct_students_dairy{
  char surname[MAX_LEN];
  char group[MAX_LEN2];
  char organization[MAX_LEN2];
  int rating;
};
int dairys_comparator(const void *val1, const void *val2){
  students_dairy *dairy1 = *(students_dairy **)val1;
  students_dairy *dairy2 = *(students_dairy **)val2;
  int result = dairy2->rating - dairy1->rating;
  printf("%d\n", result);
  return result;
}
students_dairy **dairys_read(int q){
  students_dairy **dairys = (students_dairy **)malloc(sizeof(students_dairy)*q);
  for (int i = 0; i < q; i++) {
    dairys[i] = (students_dairy *)malloc(sizeof(students_dairy));
    input_dairy(dairys[i]);
  }
  return dairys;
}
void dairys_print(students_dairy **dairys, int q) {
  printf("\n OUTPUT:\n");
  for (int i = 0; i < q; i++) {
    char surname[MAX_LEN + 12];
    char group[MAX_LEN + 9];
    char organization[MAX_LEN + 17];
    char rating[15];
    sprintf(surname, "Surname: %s; ", dairys[i]->surname);
    sprintf(group, "group: %s; ", dairys[i]->group);
    sprintf(organization, "organization: %s; ", dairys[i]->organization);
    sprintf(rating, "rating: %d; ", dairys[i]->rating);
    printf("%d) %s%s%s%s\n", (i + 1), surname, group, organization, rating);
  }
}
void input_dairy(students_dairy *dairy){
  printf("Input surname: ");
  scanf(" %[^\n]", dairy->surname);
  printf("Input group: ");
  scanf(" %[^\n]", dairy->group);
  printf("Input organization: ");
  scanf(" %[^\n]", dairy->organization);
  printf("Input rating: ");
  scanf("%d", &dairy->rating);
}
void dairys_drop(students_dairy **dairys, int q){
  for (int i = 0; i < q; i++) {
    free(dairys[i]);
  }
  free(dairys);
}
