#include <stdlib.h>
#include <unistd.h>
#include "scout.h"
#include "map.h"
#define  K_DIRECTIONS 4
#define  K_DIMENTIONS 2

void scout_toMove(scouts *scout, int *xy){
  scout->coord[0] = scout->coord[0] + xy[0];
  scout->coord[1] = scout->coord[1] + xy[1];
}
int scout_toLookForEnemies(maps *map, scouts *scout){
  if (map->map[scout->coord[0]][scout->coord[1]] == 1){
    scout->enemies_counter++;
    return 1;
  }
  return 0;
}
void scout_toFree(scouts *scout) {
  free(scout->coord);
  free(scout->direction);
  free(scout);
}
scouts *scout_toCreateNInit(maps *map) {
  scouts *scout = (scouts *)malloc(sizeof(scouts));
  scout->coord = place(map);
  scout->direction = chooseDirection(map, scout);
  scout->enemies_counter = 0;
  return scout;
}
int *place(maps *map) {
  srand(getpid());
  int *coords = (int *)malloc(sizeof(int) * K_DIMENTIONS);
  coords[0] = rand() % map->m;
  coords[1] = rand() % map->n;
  return coords;
}
int *chooseDirection(maps *map, scouts *scout){
  // Look where we are and return direction
  int paths[K_DIRECTIONS];
  paths[0] = scout->coord[0]; // up
  paths[1] = map->n - scout->coord[1];
  paths[2] = map->m - scout->coord[0];
  paths[3] = scout->coord[1];
  int max = 0;
  for (int i = 1; i < K_DIRECTIONS; i++) {
    if (paths[i] > paths[max]) {
      max = i;
    }
  }
  int *direction = (int *)malloc(sizeof(int) * K_DIMENTIONS);
  switch (max) {
    case 0:
      direction[0] = -1;
      break;
    case 1:
      direction[1] = 1;
      break;
    case 2:
      direction[0] = 1;
      break;
    case 3:
      direction[1] = -1;
      break;
  }
  return direction;
}
