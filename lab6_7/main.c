#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/wait.h>
#include "map.h"
#include "scout.h"
#define  HEIGHT 20
#define  WIDTH 30
#define  K_SCOUTS 10
#define  K_DIMENTIONS 2

int map_handler(pid_t *chpids, maps *map, int *fd);
int scout_handler(maps *map);
void send_pipes(sigset_t set, int *fd, int msg);
int recieve_pipes(int *fd);
int check_childs(pid_t *chpids);
void highlight_enemies(scouts *scout);

int main(int argc, char const *argv[]) {
  maps *map = (maps *)malloc(sizeof(maps));
  map->m = HEIGHT;
  map->n = WIDTH;
  map->map = map_new(map->m, map->n);
  map_toPrint(map);

  int fd[2];
  pipe(fd);

  sigset_t set; // List of expected signals
  sigemptyset(&set); // Init set
  sigaddset(&set, SIGUSR1); // Add signal USR1 to set

  pid_t *chpids = (pid_t *)malloc(sizeof(pid_t) * K_SCOUTS);
  pid_t chpid;
  for (int i = 0; i < K_SCOUTS; i++) {
    chpid = fork();
    if (chpid == 0) {
      // printf("CHILD(%ld): Forked!\n", (long)getpid());
      close(fd[0]);
      send_pipes(set, fd, scout_handler(map));
      return 0;
    } else {
      chpids[i] = chpid;
    }
  }
  close(fd[1]);
  return map_handler(chpids, map, fd);
}

int map_handler(pid_t *chpids, maps *map, int *fd){
  int all_enemies = 0;

  for (int i = 0; i < K_SCOUTS; i++) {
    all_enemies += recieve_pipes(fd);
  }
  printf("PARENT: Total enemies: %d.\n", all_enemies);
  while (check_childs(chpids)) { }
  printf("PARENT: All processes are exited!\n");

  free(chpids);
  map_toFree(map);
  return 0;
}
int scout_handler(maps *map){
  scouts *scout = scout_toCreateNInit(map);
  // printf("CHILD(%ld): Scout created! Placed at (%d:%d)\n", (long)getpid(),
  //                                                          scout->coord[0],
  //                                                          scout->coord[1]);

  do {
    if (scout_toLookForEnemies(map, scout)) {
      // highlight_enemies(scout);
    }
    scout_toMove(scout, scout->direction);
  } while(map_isEdge(map, scout->coord) != 1);
  if (scout_toLookForEnemies(map, scout)) {
    // highlight_enemies(scout);
  }

  // printf("CHILD(%ld): I found %d enemies!\n", (long)getpid(),
  //                                             scout->enemies_counter);
  scout_toFree(scout);
  return scout->enemies_counter;
}
void send_pipes(sigset_t set, int *fd, int msg) {
  int sig;
  if(lockf(fd[1], F_LOCK, 0) < 0){ // Lock file descriptor
    perror("SETLOCK");
  }
  pid_t mypid = getpid();
  int buffer[3];
  buffer[0] = msg;
  buffer[1] = (int)((long)mypid % 256); // Put pid in buffer...
  buffer[2] = (int)((long)mypid / 256); // ... like 2 integer
  write(fd[1], buffer, (sizeof(int) * 3));
  sigwait(&set, &sig); // Wait for a signal by parent, recieved our msg
  if(lockf(fd[1], F_ULOCK, 0) < 0){ // Unlock file descriptor...
    perror("F_SETLOCK");           // ... (when we recieved signal)
  }
}
int recieve_pipes(int *fd) {
  int buffer[3];
  read(fd[0], &buffer, sizeof(int) * 3);
  pid_t anspid = (pid_t)((long)buffer[1] +
                         (long)buffer[2] * 256); // Get pid by 2 int
  printf("PARENT: (%ld) found %d enemies!\n", (long)anspid, buffer[0]);
  kill(anspid, SIGUSR1); // Send signal to child, that sent msg, by pid
  return buffer[0];
}
int check_childs(pid_t *chpids){
  int status;
  int statuses = K_SCOUTS;
  for (int i = 0; i < K_SCOUTS; i++) {
      waitpid(chpids[i], &status, WNOHANG | WUNTRACED);
      if (kill(chpids[i], 0)){
        statuses--;
      }
  }
  return statuses;
}
void highlight_enemies(scouts *scout) {
    printf("CHILD(%ld): I found enemy! (%d;%d)\n", (long)getpid(),
                                                   scout->coord[0],
                                                   scout->coord[1]);
}
