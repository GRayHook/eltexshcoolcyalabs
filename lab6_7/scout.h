#ifndef SCOUT_H
#define SCOUT_H

#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include "map.h"

struct strct_scout{
  pid_t pid;
  pid_t ppid;
  int *coord;
  int *direction;
  int enemies_counter;
};
typedef struct strct_scout scouts;

int scout_toLookForEnemies(maps *map, scouts *scout);
void scout_toMove(scouts *scout, int *xy);
scouts *scout_toCreateNInit(maps *map);
void scout_toFree(scouts *scout);
int *place(maps *map);
int *chooseDirection(maps *map, scouts *scout);

#endif
