#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "map.h"
#define  K_DIMENTIONS 2
#define  MAP_CHANCE 90

int map_isEdge(maps *map, int *coordsv){
  if (coordsv[0] == (map->m - 1) || coordsv[0] == 0) {
    return 1;
  }
  if (coordsv[1] == (map->n - 1) || coordsv[1] == 0) {
    return 1;
  }
  return 0;
}
void map_toPrint(maps *map) {
  for (int i = 0; i < map->m; i++) {
    for (int j = 0; j < map->n; j++) {
      printf("%d ", map->map[i][j]);
    }
    printf("\n");
  }
}
void map_toPrint_higlight(maps *map, int **coordsv, int coordsc) {
  int xy[K_DIMENTIONS];
  for (int i = 0; i < map->m; i++) {
    xy[0] = i;
    for (int j = 0; j < map->n; j++) {
      xy[1] = j;
      if (inArr(xy, coordsv, coordsc)) {
        printf("[%d]", map->map[i][j]);
      } else {
        printf(" %d ", map->map[i][j]);
      }
    }
    printf("\n");
  }
}
int inArr(int *xy, int **coordsv, int coordsc){
  for (int i = 0; i < coordsc; i++) {
    if (xy[0] == coordsv[i][0] && xy[1] == coordsv[i][1]) {
      return 1;
    }
  }
  return 0;
};
int **map_new(int m, int n){
  int **map = (int **)malloc(sizeof(int *) * m);
  for (int i = 0; i < m; i++) {
    map[i] = (int *)malloc(sizeof(int) * n);
  }
  map_toGenerate(map, m, n);
  return map;
}
void map_toFree(maps *map) {
  for (int i = 0; i < map->m; i++) {
    free(map->map[i]);
  }
  free(map->map);
}
void map_toGenerate(int **map, int m, int n){
  int random;
  srand(getpid());
  for (int i = 0; i < m; i++) {
    for (int j = 0; j < n; j++) {
      random = rand() % 100;
      map[i][j] = map_rand(random);
    }
  }
}
int map_rand(int random) {
  if (random > MAP_CHANCE) {
    return 1;
  } else {
    return 0;
  }
}
