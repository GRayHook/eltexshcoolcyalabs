#ifndef MAP_H
#define MAP_H

struct strct_map {
  int **map;
  int m;
  int n;
};
typedef struct strct_map maps;

int map_isEdge(maps *map, int *coordsv);
void map_toPrint_higlight(maps *map, int **coordsv, int coordsc);
void map_toPrint(maps *map);
int inArr(int *xy, int **coordsv, int coordsc);
int **map_new(int m, int n);
void map_toFree(maps *map);
void map_toGenerate(int **map, int m, int n);
int map_rand(int random);

#endif
