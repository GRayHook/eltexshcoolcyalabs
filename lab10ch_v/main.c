#include "main.h"

int main(int argc, char const *argv[]) {
  initscr();
  refresh();
  pthread_t threads[K_WORKERS + 1];
  for (int i = 0; i < K_WORKERS; i++) {
    ids[i] = i + 1;
    nanosleep (&tw3, &tr);
    pthread_create(&threads[i], NULL, to_work, (void *)&ids[i]);
  }
  pthread_create(&threads[K_WORKERS], NULL, to_ctl, NULL);
  for (int i = 0; i < K_WORKERS + 1; i++) {
    pthread_join(threads[i], NULL);
  }
  getch(); // Wait for any key
  endwin();
  exit(0);
}

void *to_work(void *args) {
  int myid = *(int *)args;
  int counter = 0;
  int sock;
  struct sockaddr_in addr;
  char txt[10] = "";
  sprintf(txt, "%d)", myid);
  print_ncur(txt, myid + 2, 5);
  nanosleep (&tw, &tr);
  nanosleep (&tw2, &tr);
  sock = socket(AF_INET, SOCK_STREAM, 0);
  if(sock < 0) {
    endwin();
    perror("socket");
    exit(1);
  }
  addr.sin_family = AF_INET;
  addr.sin_port = htons(SOCKET_PORT);
  addr.sin_addr.s_addr = inet_addr("192.168.1.219");
  if(connect(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
      endwin();
      perror("connect");
      exit(2);
  }
  print_ncur((char *)"con!", myid+2, 60);
  nanosleep (&tw, &tr);
  nanosleep (&tw, &tr);
  print_ncur((char *)"pthread started!", myid+2, 40);
  int pipeline_c_local = 1;
  while(1){
    if ((pipeline_c_local - counter) < K_WORKERS + 1) {
      print_ncur((char *)"sending!", myid+2, 15);
      pipeline_ans ans = send_detail(sock, myid);
      print_ncur((char *)"sent!   ", myid+2, 15);
      pipeline_c_local = ans.c;
      if (ans.success) {
        print_ncur((char *)"success!", myid+2, 15);
        counter = ans.c;
      }
      nanosleep (&tw, &tr);
    } else {
      nanosleep (&tw, &tr);
      print_ncur((char *)"GUF! ", myid+2, 15);
      // if (pthread_mutex_lock(&mutext) == 0) { // NOTE: mb, bring it to sockets
      //   mvwprintw(stdscr, myid+2, 1, "%d guf", myid);
      //   pthread_mutex_unlock(&mutext);
      // }
      close(sock);
      return 0;
    }
  }
  print_ncur((char *)"pthread stopped!", myid+2, 40);
  return 0;
}

void *to_ctl(void *args) {
  int sock[K_WORKERS], listener;
  struct sockaddr_in addr;
  listener = socket(AF_INET, SOCK_STREAM, 0);
  if(listener < 0) {
      endwin();
      perror("socket");
      exit(1);
  }
  addr.sin_family = AF_INET;
  addr.sin_port = htons(SOCKET_PORT);
  addr.sin_addr.s_addr = INADDR_ANY;
  if(bind(listener, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
    endwin();
    perror("bind");
    exit(2);
  }
  listen(listener, K_WORKERS);
  pthread_t handlers[K_WORKERS];
  for (int i = 0; i < K_WORKERS; i++) {
    sock[i] = accept(listener, NULL, NULL);
    print_ncur((char *)"Acc!", i+3, 70);
    if(sock < 0) {
      endwin();
      perror("accept");
      exit(3);
    }
    print_ncur((char *)"!", 11, i);
    print_ncur((char *)".", 9, sock[i]%80);
    pthread_create(&handlers[i], NULL, ctlr_handler, &sock[i]);
    print_ncur((char *)"pthread created!", i+3, 40);
  }
  close(listener);

  nanosleep (&tw, &tr);
  nanosleep (&tw2, &tr);
  nanosleep (&tw, &tr);
  nanosleep (&tw2, &tr);
  nanosleep (&tw, &tr);
  nanosleep (&tw2, &tr);
  while(1) { // Controller's job:
    nanosleep (&tw2, &tr);
    if (isnt_empty_pipeline()) {
      if (pthread_mutex_lock(&mutext) == 0) {
        to_inspect_detail();
        mvwprintw(stdscr, 1, 1, "Workers are goods! %d: ", pipeline_c);
        pipeline_print();
        refresh();
        pthread_mutex_unlock(&mutext);
      }
    } else {
      mvwprintw(stdscr, 1, 1, "Workers are FOOLS! %d: ", pipeline_c);
      pipeline_print();
      refresh();
      for (int i = 0; i < K_WORKERS; i++) {
        pthread_join(handlers[i], NULL);
      }
      return 0;
    }

  }
}
void *ctlr_handler(void *args) { // Handler for controller,
                                 // that must recieve ids from workers
  int sock = *(int *)args;
  int buf;
  int bytes_read;
  while (1) {
    char txt[10] = "txt";
    print_ncur((char *)".", 10, sock%80);
    bytes_read = recv(sock, &buf, sizeof(int), 0);
    sprintf(txt, "recieve %d", bytes_read);
    print_ncur(txt, buf+2, 27);
    if(bytes_read <= 0) break;
    pipeline_ans buf_a = {pipeline_c, 0};
    buf_a.success = put_on_pipeline(buf);
    send(sock, &buf_a, sizeof(pipeline_ans), 0);
  }
  return 0;
}

void pipeline_print() {
  for (int i = 0; i < K_PLACES; i++) {
    mvwprintw(stdscr, 1, 25+(i*2), "%d",pipeline[i]);
  }
}
int put_on_pipeline(int id) { // Worker put detail on pipeline
  for (int i = 0; i < K_PLACES; i++) {
    if (!pipeline[i]) {
      pipeline[i] = id;
      return 1;
    }
  }
  return 0;
}
pipeline_ans send_detail(int sock, int id) { // Send detail by socket
  pipeline_ans buf;
  print_ncur((char *)"sending!", id+2, 27);
  send(sock, &id, sizeof(int), 0);
  print_ncur((char *)"sent!   ", id+2, 27);
  recv(sock, &buf, sizeof(pipeline_ans), 0);
  print_ncur((char *)"recv!   ", id+2, 27);
  return buf;
}
int isnt_empty_pipeline() {
  for (int i = 0; i < K_PLACES; i++) {
    if (pipeline[i]) {
      return 1;
    }
  }
  return 0;
}
void to_inspect_detail() {
  pipeline_c++; // Tick
  for (int i = 0; i < K_PLACES - 1; i++) {
    if (1[pipeline + i]) {
      pipeline[i] = 1[pipeline + i];
    } else {
      pipeline[i] = 0;
    }
  }
  pipeline[K_PLACES - 1] = 0;
}

void print_ncur(char *txt, int x, int y){
  if (pthread_mutex_lock(&mutext) == 0) {
    mvwprintw(stdscr, x, y, "%s", txt);
    refresh();
    pthread_mutex_unlock(&mutext);
  }
}
