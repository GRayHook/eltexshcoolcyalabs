#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <ncurses.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#define  K_WORKERS 5
#define  K_PLACES 10
#define  T_DELAY1 700000000
#define  T_DELAY2 300000000
#define  T_DELAY3  100000
#define  SOCKET_PORT 51118

int ids[K_WORKERS];
int pipeline[K_PLACES];
int pipeline_c = 0;
pthread_mutex_t mutext = PTHREAD_MUTEX_INITIALIZER;
struct timespec tr;
struct timespec tw = {0,T_DELAY1};
struct timespec tw2 = {0,T_DELAY2};
struct timespec tw3 = {0,T_DELAY3};
typedef struct struct_pipeline_ans { int c; int success; } pipeline_ans;

void *to_work(void *args);

void *to_ctl(void *args);
void *ctlr_handler(void *args);

void pipeline_print();
int put_on_pipeline(int id);
pipeline_ans send_detail(int sock, int id);
int isnt_empty_pipeline();
void to_inspect_detail();

void print_ncur(char *txt, int x, int y);
