#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define  MAX_LEN 1000
#define  MAX_LEN2 300
#define  INPUT ".input"
#define  OUT ".out"

char *change_name(char *str4ka);
FILE *right_fopen(char *path, char mode);
void spaceIt(FILE *fp_inp, FILE *fp_out, int count);

int main(int argc, char const *argv[]) {
  char *file = (char *)argv[1];
  int count = atoi(argv[2]);
  FILE *fp_inp;
  FILE *fp_out;

  fp_inp = right_fopen(file, 'r');
  fp_out = right_fopen(change_name(file), 'w');

  spaceIt(fp_inp, fp_out, count);

  fclose(fp_inp);
  fclose(fp_out);
  return 0;
}

void spaceIt(FILE *fp_inp, FILE *fp_out, int count) {
  char ch, first_char = ' ';
  while((ch=fgetc(fp_inp)) != EOF) {
    if ((ch == ' ')  && (count > 0)) {
      putc(first_char, fp_out);
      count--;
    } else {
      if (first_char == ' ') {
        first_char = ch;
      }
      putc(ch, fp_out);
    }
  }
}
char *change_name(char *str4ka){
  char *str4ka2 = (char *)malloc(sizeof(char *) * MAX_LEN2);
  strcpy(str4ka2, str4ka);
  strcpy(strstr(str4ka2, INPUT), OUT);
  return str4ka2;
}
FILE *right_fopen(char *path, char mode) {
  FILE *fp;
  if((fp=fopen(path, &mode))==NULL) {
    printf("Unable open file (output).\n");
    exit(1);
  }
  return fp;
}
